Dependencies
============

* C compiler: c99+
* libC
* SDL 2.0+

Known to work with
------------------
* GCC 12.2, 13.2.1: -std=c99, -std=c11, -std=gnu2x
* CLang 17.0.6
* Glibc 2.36, 2.38
* SDL 2.28.3, 2.28.5

