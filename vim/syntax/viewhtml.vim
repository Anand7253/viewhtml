" vim/syntax/viewhtml.c
runtime! syntax/c.vim
unlet b:current_syntax

syntax keyword cType NodeType FlowType Size Rect2 Sides string_view FlowLine FloatData Node ParentStruct TextStruct RectStruct Inline Block FloatStruct FlowRoot
syntax keyword cType SDL_Window SDL_Renderer SDL_Rect SDL_Point SDL_Event SDL_Color SDL_HitTestResult
syntax keyword cConstant NODETYPE_INTERNAL NODETYPE_TEXT NODETYPE_FLOWROOT FLOWTYPE_NOT_FLOW FLOWTYPE_RECT FLOWTYPE_INLINE FLOWTYPE_BLOCK FLOWTYPE_FLOAT
syntax keyword cConstant DEBUG LOG INTERWORD INTERLINE BORDER containedin=cPreCondit
syntax keyword cConstant SDL_QUIT
syntax keyword cAllocFunction calloc AllocParentMultisize_One AllocParentMultisize_LenPlusOne AllocFlowrootFloatdata_LenPlusOne AllocRootAllLines_LenPlusOne
syntax keyword cFunction RenderRect Flow_InsertNewLine Flow_AfterNewLineInserted Flow_GetFloatMargin Flow_GetShortFloatMargin ResizeText DrawText Flow_ResizeRect Flow_DrawRect Flow_ResizeInline Flow_DrawInline Flow_ResizeBlock Flow_DrawBlock Flow_ResizeFloat Flow_DrawFloat Free_ResizeFlowRoot Flow_ResizeNode Flow_DrawNode Flow_ResizeNodes Flow_DrawNodes ResizeFlowRoot DrawFlowRoot DestroyNode simpleHTMLParser HitTest main
syntax keyword cAssert assert

syntax region cDimmedArguments start=/\%(\<__attribute__\|\<assert\)\@<=\s*(/ end=// contains=cDimmedArgumentsParen
syntax region cDimmedArgumentsParen start=/(/ end=/)/ transparent contains=cDimmedArgumentsParen contained containedin=cDimmedArguments
syntax cluster cParenGroup add=cDimmedArgumentsParen

highlight default link cAllocFunction   cConstant
highlight default link cFunction        Function
highlight default link cAssert          cStatement
highlight default      Dimmed           ctermfg=243
highlight default link cDimmedArguments Dimmed

let b:current_syntax = 'viewhtml'
