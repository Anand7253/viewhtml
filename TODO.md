To Do List
==========

Backward branches
* Error message when no graphical session can be accessed.

Backward sideway features
* text direction
* check screen size at window creation

Sideway features
* Terminal User Interface
* Accessibility: screen readers, special keyboards / joysticks, etc

Forward features
* Flow:
  - illegible rectangle
* HTML Flex
* HTML Table
* HTML Grid
* text
* overflow

Fast forward features
* CSS, configuration sheets

