/* Copyleft (C) Gnu GPL 3.0+ 2022, 2023, 2024
 * Anand7253 <anand_ro@protonmail.com> <@anand7253@fosstodon.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but without any warranty; without even the implied warranty of
 * merchantability or fitness for a perticular purpose.  See the
 * GNU General Public License for more details.
 *
 * If you haven't received a copy of the GNU General Public License
 * along with this program, see <https://www.gnu.org/licenses/>. */

/* viewhtml.c */

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>


typedef enum NodeType {
	NODETYPE_INTERNAL, /* flowtype != 0 */ /* internal to some algorithm, eg. flow */
	NODETYPE_TEXT,     /* flowtype is one of NOT_FLOW, RECT, FLOAT */
	NODETYPE_FLOWROOT  /* flowtype is one of NOT_FLOW, RECT, FLOAT */
} NodeType;

typedef enum FlowType {
	FLOWTYPE_NOT_FLOW, /* nodetype != 0 */
	FLOWTYPE_RECT,     /* nodetype != 0 */
	FLOWTYPE_INLINE,   /* nodetype == 0 */
	FLOWTYPE_BLOCK,    /* nodetype == 0 */
	FLOWTYPE_FLOAT     /* nodetype != 0 */
} FlowType;


typedef struct Size {
	int w, h;
} Size;

typedef struct Rect2 {
	int x1, y1, x2, y2;
} Rect2;

typedef struct Sides {
	int left, right, top, bottom;
} Sides;

typedef struct string_view {
	const char *start, *end;
} string_view;

typedef struct FlowLine {
	int w, h;
	int floatmargin_left, floatmargin_right, floatmargin_top;
	int wordbreaks; /* The number of spaces in between words */
} FlowLine;

typedef struct FloatData {
	int x1; /* relative to flow-root's content-box, not useful for in-line nodes */
	int y1; /* counting from the top of the flow-root's content-box
	         * When the most significant bit is set, it has to be unset and the height of the current flow-line needs to be added to y1 and y2 */
	int x2; /* Width to be added to the starting point (x1) or be taken from the ending point (x2) of the flow-root's content-box
	         * It will be bigger than the width of the float if the float is not touching the side of the flow-root, eg. because of another earlier float or being nested in a block */
	int y2;
	struct FloatStruct *node;
} FloatData;


typedef struct Node Node;
typedef struct ParentStruct ParentStruct;

typedef struct TextStruct {
	Node       *node;
	Size        size;
} TextStruct;

typedef struct RectStruct {
	bool        start_with_nl;
} RectStruct;

struct ParentStruct {
	Node       *node;
	string_view name;
	Node      **nodes;
	int         nodes_count;
	Sides       exospace;
	FlowLine   *multisize;
	int         multisize_len;
	int         multisize_capacity;
};

/* inherits ParentStruct */
typedef struct Inline {
	Node       *node;
	string_view name;
	Node      **nodes;
	int         nodes_count;
	Sides       exospace;
	FlowLine   *multisize; /* borderbox multisize */
	int         multisize_len;
	int         multisize_capacity;
	bool        start_with_nl;
} Inline;

/* inherits ParentStruct */
typedef struct Block {
	Node       *node;
	string_view name;
	Node      **nodes;
	int         nodes_count;
	Sides       exospace;
	FlowLine   *inner_multisize;
	int         inner_multisize_len;
	int         inner_multisize_capacity; /* how much memory is allocated */
	int         inner_multisize_id; /* currently resized/rendered/drawn line's id */
	int        *linestart_nodepos; /* vector of child ids */
	int         linestart_nodepos_len;
	int         linestart_nodepos_capacity;
	Size        content_size; /* content-box size, for resizing */
	SDL_Rect    rect;        /* x, y: position of the next rect to be rendered inline, for resizing and drawing, includes line.floatmargin_left in drawing functions *
	                          * w, h: w, h of the currently drawn inline rect, used localy by drawing functions */
	Size        borderbox;
} Block;

typedef struct FloatStruct {
	Node       *node;
	int         floatmargin_left;
	int         floatmargin_top;
} FloatStruct;

/* inherits ParentStruct and Block */
typedef struct FlowRoot {
	Node       *node;
	string_view name;
	Node      **nodes;
	int         nodes_count;
	Sides       exospace;
	FlowLine   *inner_multisize;
	int         inner_multisize_len;
	int         inner_multisize_capacity; /* how much memory is allocated */
	int         inner_multisize_id; /* currently resized/rendered/drawn line's id */
	int        *linestart_nodepos; /* vector of child ids */
	int         linestart_nodepos_len;
	int         linestart_nodepos_capacity;
	Size        content_size; /* content-box size, for resizing */
	SDL_Rect    rect;      /* for resizing and drawing */
	Size        borderbox;
	SDL_Point   content_pos;
	FloatData  *floatdata; /* sorted by (a) y1 ASC and (b) x ASC */
	int         floatdata_len;
	int         floatdata_capacity;
	SDL_Rect   *all_lines;
	int         all_lines_len;
	int         all_lines_capacity;
} FlowRoot;

struct Node {
	NodeType nodetype; /* like inner-display */
	/* union { */ /* like outer-display */
	FlowType flowtype;
	/* }; */
	ParentStruct *parent;
	int           child_id;
	bool          debug_this_node;
	TextStruct   *t;
	RectStruct    fw; /* w stands for word */
	ParentStruct *fp;
	Inline       *fi;
	Block        *fb;
	FlowRoot     *fr;
	FloatStruct  *ff;
};


// #define DEBUG
// #define LOG

#ifndef INTERWORD
#define INTERWORD 12
#endif
#ifndef INTERLINE
#define INTERLINE 5
#endif
#ifndef BORDER
#define BORDER 10
#endif

#define max(a, b) ((a) > (b) ? (a) : (b))


bool debug_log;


void RenderRect (SDL_Renderer *rend, SDL_Rect outer_size, int width, SDL_Color color) __attribute__ (( nonnull(1) ));

/* During resizing:
 * block->rect.x is counted from the left of the content-box, does not include floatmargin_left
 * block->content_size.w is the width of the content-box, this excludes the margin, border and padding
 * block->borderbox is the size of the border-box
 * exospace = block and inline nodes' borders + paddings
 * exospace.w = right only b&ps
 * exospace.h = top + bottom b&ps */
/* During drawing:
 * block->rect.x is counted from the left of the flow root content-box, includes floatmargin_left
 * exospace = block and inline nodes' left + top borders + paddings */

void AllocParentMultisize_One          (ParentStruct *parent)                                      __attribute__ (( nonnull(1) ));
void AllocParentMultisize_LenPlusOne   (ParentStruct *parent)                                      __attribute__ (( nonnull(1) ));
void AllocFlowrootFloatdata_LenPlusOne     (FlowRoot *root)                                        __attribute__ (( nonnull(1) ));
void AllocRootAllLines_LenPlusOne          (FlowRoot *root)                                        __attribute__ (( nonnull(1) ));
void Flow_InsertNewLine                    (FlowRoot *root, Block *block, Node *child)             __attribute__ (( nonnull(1, 2, 3) ));
void Flow_AfterNewLineInserted             (FlowRoot *root, Block *block)                          __attribute__ (( nonnull(1, 2) ));
int  Flow_GetFloatMargin             (const FlowRoot *root, const Block *block, int y, int height) __attribute__ (( nonnull(1) )); /* block may by NULL */
int  Flow_GetShortFloatMargin        (const FlowRoot *root, int y_min, int height, int width_max)  __attribute__ (( nonnull(1) ));

void ResizeText                            (TextStruct *rect)                                      __attribute__ (( nonnull(1) ));
void DrawText          (SDL_Renderer *rend, TextStruct *rect, SDL_Point borderpos)                 __attribute__ (( nonnull(1, 2) ));

void Flow_ResizeRect                       (FlowRoot *root, Block *block, Node *rect)              __attribute__ (( nonnull(1, 2, 3) ));
void Flow_DrawRect     (SDL_Renderer *rend, FlowRoot *root, Block *block, Node *rect)              __attribute__ (( nonnull(1, 2, 3, 4) ));
void Flow_ResizeInline                     (FlowRoot *root, Block *block, Inline *in_line)         __attribute__ (( nonnull(1, 2, 3) ));
void Flow_DrawInline   (SDL_Renderer *rend, FlowRoot *root, Block *block, Inline *in_line)         __attribute__ (( nonnull(1, 2, 3, 4) ));
void Flow_ResizeBlock                      (FlowRoot *root, Block *block, Block *inner)            __attribute__ (( nonnull(1, 2, 3) ));
void Flow_DrawBlock    (SDL_Renderer *rend, FlowRoot *root, Block *block, Block *inner)            __attribute__ (( nonnull(1, 2, 3, 4) ));
void Flow_ResizeFloat                      (FlowRoot *root, Block *block, FloatStruct *inner)      __attribute__ (( nonnull(1, 2, 3) ));
void Flow_DrawFloat    (SDL_Renderer *rend, FlowRoot *root, Block *block, FloatStruct *inner)      __attribute__ (( nonnull(1, 2, 3, 4) ));
void Free_ResizeFlowRoot                   (FlowRoot *inner)                                       __attribute__ (( nonnull(1) ));

void Flow_ResizeNode                       (FlowRoot *root, Block *block, Node *inner)                    __attribute__ (( nonnull(1, 2, 3) ));
void Flow_DrawNode     (SDL_Renderer *rend, FlowRoot *root, Block *block, Node *inner)                    __attribute__ (( nonnull(1, 2, 3, 4) ));
void Flow_ResizeNodes                      (FlowRoot *root, Block *block, Node *nodes[], int nodes_count) __attribute__ (( nonnull(1, 2) ));
void Flow_DrawNodes    (SDL_Renderer *rend, FlowRoot *root, Block *block, Node *nodes[], int nodes_count) __attribute__ (( nonnull(1, 2, 3) ));

void ResizeFlowRoot                        (FlowRoot *block)                                              __attribute__ (( nonnull(1) ));
void DrawFlowRoot      (SDL_Renderer *rend, FlowRoot *block)                                              __attribute__ (( nonnull(1, 2) ));
void DestroyNode                           (Node *node)                                                   __attribute__ (( nonnull(1) ));

Node *simpleHTMLParser (const char *simpleHTML);


void RenderRect (SDL_Renderer *rend, SDL_Rect outer_rect, int width, SDL_Color color) {
	SDL_Rect rects[4] = {
		{outer_rect.x, outer_rect.y, outer_rect.w, width}, /* top bar with corners */
		{outer_rect.x, outer_rect.y + outer_rect.h - width, outer_rect.w, width}, /* bottom bar with corners */
		{outer_rect.x, outer_rect.y + width, width, outer_rect.h - 2 * width}, /* left bar without corners */
		{outer_rect.x + outer_rect.w - width, outer_rect.y + width, width, outer_rect.h - 2 * width}}; /* right bar without corners */
	SDL_SetRenderDrawColor(rend, color.r, color.g, color.b, color.a);
	SDL_RenderFillRects(rend, rects, 4);
}

/* also resizes and initializes the new element */
void AllocParentMultisize_One (ParentStruct *parent) {
	if (!parent->multisize_len) {
		if (!parent->multisize_capacity) {
			assert(parent->multisize == NULL);
			parent->multisize = malloc(1 * sizeof(FlowLine));
			parent->multisize_capacity = 1;
		}
		parent->multisize_len = 1;
		parent->multisize[parent->multisize_len - 1] = (FlowLine){.w = 0, .h = 0, .floatmargin_left = 0, .floatmargin_right = 0, .floatmargin_top = 0, .wordbreaks = 0};
	}
}

/* also resizes and initializes the new element */
void AllocParentMultisize_LenPlusOne (ParentStruct *parent) {
	if (parent->multisize_len == parent->multisize_capacity) {
		parent->multisize_capacity ++;
		parent->multisize = realloc(parent->multisize, parent->multisize_capacity * sizeof(FlowLine));
	}
	parent->multisize_len ++;
	parent->multisize[parent->multisize_len - 1] = (FlowLine){.w = 0, .h = 0, .floatmargin_left = 0, .floatmargin_right = 0, .floatmargin_top = 0, .wordbreaks = 0};
}

void AllocFlowrootFloatdata_LenPlusOne (FlowRoot *root) {
	if (root->floatdata_len == root->floatdata_capacity) {
		root->floatdata_capacity ++;
		root->floatdata = realloc(root->floatdata, root->floatdata_capacity * sizeof(FloatData));
	}
	root->floatdata_len ++;
}

/* also resizes the new element */
void AllocRootAllLines_LenPlusOne (FlowRoot *root) {
	if (root->all_lines_len == root->all_lines_capacity) {
		root->all_lines_capacity ++;
		root->all_lines = realloc(root->all_lines, root->all_lines_capacity * sizeof(FlowLine));
	}
	root->all_lines_len ++;
}

/* resize phase only */
void Flow_InsertNewLine (FlowRoot *root, Block *block, Node *child) {
	ParentStruct *child_parent = NULL; /* child, but of the type ParentStruct */
	ParentStruct *parent = child->parent;
	bool is_empty_area = true;
#ifdef LOG
	int it_id = 0;
	bool debug_me = child->debug_this_node && debug_log;
#endif

	while (child && parent) {
#ifdef LOG
		if (debug_me) {
			printf("[%d] child = {flowtype = %d, fp = ", it_id, child->flowtype);
			if (child_parent) {
				printf("{name = \33[4m%.*s\33[m, size = [", (int)(child_parent->name.end - child_parent->name.start), child_parent->name.start);
				for (int i = 0; i < child_parent->multisize_len; ++i) {
					if (i)
						printf(", ");
					printf("{w=%d, h=%d}", child_parent->multisize[i].w, child_parent->multisize[i].h);
				}
				printf("]}");
			}
			printf("}, parent = {name = \33[4m%.*s\33[m, size = [", (int)(parent->name.end - parent->name.start), parent->name.start);
			for (int i = 0; i < parent->multisize_len; ++i) {
				if (i)
					printf(", ");
				printf("{w=%d, h=%d}", parent->multisize[i].w, parent->multisize[i].h);
			}
			printf("]}\n");
		}
		++it_id;
#endif
		assert(parent->node->flowtype == FLOWTYPE_INLINE || parent->node->flowtype == FLOWTYPE_BLOCK || parent->node->nodetype == NODETYPE_FLOWROOT);

		if (is_empty_area && parent->node->flowtype == FLOWTYPE_INLINE && (!child->child_id || !parent->multisize_len || !parent->multisize[0].w))
			((Inline*)parent)->start_with_nl = true;
		else
			is_empty_area = false; /* break */

		if (child_parent && child_parent->multisize_len >= 2) {
			AllocParentMultisize_One(parent); /* This is immediately filled with child_parent's second-last line */
			assert(child_parent->multisize[child_parent->multisize_len - 1].w == 0); /* There is a terminating end-of-line */
			assert(child_parent->multisize[child_parent->multisize_len - 1].h == 0); /* so there is a blank line. */
			assert(child_parent->multisize[child_parent->multisize_len - 2].w != 0); /* This line shouldn't be empty. */
			assert(child_parent->multisize[child_parent->multisize_len - 2].h != 0);

			child_parent->multisize[child_parent->multisize_len - 2].w += child_parent->exospace.left - parent->exospace.left + child_parent->exospace.right - parent->exospace.left;
			child_parent->multisize[child_parent->multisize_len - 2].h += child_parent->exospace.top - parent->exospace.top + child_parent->exospace.bottom - parent->exospace.bottom;

			//if (parent->node->flowtype == FLOWTYPE_INLINE && parent->node->child_id && !parent->node->fi->start_with_nl)
			//if (parent->multisize[parent->multisize_len - 1].w)
			//	parent->multisize[parent->multisize_len - 1].w += INTERWORD;
			parent->multisize[parent->multisize_len - 1].w += child_parent->multisize[child_parent->multisize_len - 2].w;
			if (parent->multisize[parent->multisize_len - 1].h < child_parent->multisize[child_parent->multisize_len - 2].h)
				parent->multisize[parent->multisize_len - 1].h = child_parent->multisize[child_parent->multisize_len - 2].h;
		}

		AllocParentMultisize_LenPlusOne(parent); /* Left empty, to be filled by the caller */
		if (parent->node->flowtype == FLOWTYPE_BLOCK || parent->node->nodetype == NODETYPE_FLOWROOT)
			break;
		child_parent = parent;
		child = parent->node;
		parent = child->parent;
	}

	AllocRootAllLines_LenPlusOne(root);
	root->all_lines[root->all_lines_len - 1].x = block->exospace.left + block->inner_multisize[block->inner_multisize_len - 2].floatmargin_left;
	root->all_lines[root->all_lines_len - 1].y = block->rect.y;
	root->all_lines[root->all_lines_len - 1].w = block->inner_multisize[block->inner_multisize_len - 2].w;
	root->all_lines[root->all_lines_len - 1].h = block->inner_multisize[block->inner_multisize_len - 2].h;

	return;
}

/* resize phase only */
void Flow_AfterNewLineInserted (FlowRoot *root, Block *block) {
	if (root->floatdata_len) {
		assert(block->inner_multisize_len);
		int amount = block->inner_multisize[block->inner_multisize_len - 2].h;
		for (FloatData *fl = root->floatdata + root->floatdata_len - 1; fl >= root->floatdata && fl->node->floatmargin_top == -1; fl--) {
			fl->y1 += amount;
			fl->y2 += amount;
			fl->node->floatmargin_top = amount;
		}
	}
}

int Flow_GetFloatMargin (const FlowRoot *root, const Block *block, int y, int h) {
	int floatmargin_left = 0;
	for (int i = 0; i < root->floatdata_len; ++i)
		if (root->floatdata[i].y1 < y + h && root->floatdata[i].y2 > y && floatmargin_left < root->floatdata[i].x2)
			floatmargin_left = root->floatdata[i].x2;
		else if (root->floatdata[i].y1 >= y + h)
			break;
	if (block)
		if (floatmargin_left - block->exospace.left > 0)
			return floatmargin_left - block->exospace.left;
		else
			return 0;
	else
		return floatmargin_left;
}

int Flow_GetShortFloatMargin (const FlowRoot *root, int ymin, int h, int wmax) {
	int y = ymin;
	for (int i = 0; i < root->floatdata_len; ++i)
		if (root->floatdata[i].y1 < y + h && root->floatdata[i].y2 > y && root->floatdata[i].x2 > wmax)
			y = root->floatdata[i].y2;
		else if (root->floatdata[i].y1 >= y + h)
			break;
	return y;
}

void ResizeText (TextStruct *node) {
	return;
}

void DrawText (SDL_Renderer *rend, TextStruct *node, SDL_Point borderpos) {
	SDL_Rect rect = {.x = borderpos.x, .y = borderpos.y, .w = node->size.w, .h = node->size.h};
	RenderRect(rend, rect, 2, (SDL_Color){127, 127, 127, 255});
	return;
}

void Flow_ResizeRect (FlowRoot *root, Block *block, Node *node) {
	Size borderbox;
	switch (node->nodetype) {
	case NODETYPE_TEXT:
		ResizeText(node->t);
		borderbox = node->t->size;
		break;
	case NODETYPE_FLOWROOT:
		node->fr->borderbox.w = root->content_size.w - node->parent->exospace.left - node->parent->exospace.right;
		node->fr->borderbox.h = root->content_size.h - node->parent->exospace.top - node->parent->exospace.bottom;
		Free_ResizeFlowRoot(node->fr);
		borderbox = node->fr->borderbox;
		break;
	default:
		assert(0);
	}

	block->rect.w = borderbox.w;
	block->rect.h = borderbox.h;
	Sides exospace = node->parent->exospace;
	//Sides exospace = node->parent->node->flowtype == FLOWTYPE_INLINE ? node->parent->exospace : (Sides){0,0,0,0};

	/* should insert new line? */
	int line_floatmargin_left = block->inner_multisize_len
			? block->inner_multisize[block->inner_multisize_id].h < exospace.top + borderbox.h + exospace.bottom
			  ? Flow_GetFloatMargin(root, block, block->rect.y, exospace.top + borderbox.h + exospace.bottom)
			  : block->inner_multisize[block->inner_multisize_id].floatmargin_left
			: 0;
	int interword = block->rect.x ? INTERWORD : 0;
	node->fw.start_with_nl = line_floatmargin_left + exospace.left + block->rect.x + interword + borderbox.w + exospace.right > root->content_size.w;

	if (node->fw.start_with_nl) {
		interword = 0;
		if (block->rect.x) {
			Flow_InsertNewLine(root, block, node);
			assert(block->inner_multisize_id < block->inner_multisize_len);
			block->rect.y += block->inner_multisize[block->inner_multisize_id].h + INTERLINE;
			block->inner_multisize_id ++;
			block->rect.x = 0;
			Flow_AfterNewLineInserted(root, block);
		}

		int old_rect_y = block->rect.y;
		block->rect.y = Flow_GetShortFloatMargin(root,
				block->rect.y,
				exospace.top + borderbox.h + exospace.bottom,
				block->exospace.left + block->content_size.w - exospace.left - borderbox.w - exospace.right + block->exospace.right);
		block->inner_multisize[block->inner_multisize_id].floatmargin_top = block->rect.y - old_rect_y;
		block->inner_multisize[block->inner_multisize_id].floatmargin_left = Flow_GetFloatMargin(root, block, block->rect.y, exospace.top + borderbox.h + exospace.bottom);
	}

	FlowLine child_size = {.w = borderbox.w, .h = borderbox.h, .floatmargin_left = 0, .floatmargin_right = 0};

	ParentStruct *parent = node->parent;
	assert(parent->node->flowtype == FLOWTYPE_INLINE || parent->node->flowtype == FLOWTYPE_BLOCK || parent->node->nodetype == NODETYPE_FLOWROOT);
	AllocParentMultisize_One(parent);
	if (parent->multisize[parent->multisize_len - 1].h < child_size.h)
		parent->multisize[parent->multisize_len - 1].h = child_size.h;
	if (parent->multisize[parent->multisize_len - 1].w)
		parent->multisize[parent->multisize_len - 1].w += INTERWORD;
	if (interword)
		block->inner_multisize[block->inner_multisize_len - 1].wordbreaks += 1;
	parent->multisize[parent->multisize_len - 1].w += child_size.w;

	block->rect.x += interword + borderbox.w;
	return;
}

void Flow_DrawRect (SDL_Renderer *rend, FlowRoot *root, Block *block, Node *node) {
	Size borderbox = node->nodetype == NODETYPE_TEXT ? node->t->size : node->fr->borderbox;
	block->rect.w = borderbox.w;
	block->rect.h = borderbox.h;

	if (node->fw.start_with_nl) {
		if (block->rect.x > block->inner_multisize[block->inner_multisize_id].floatmargin_left) {
			assert(block->inner_multisize_id + 1 < block->inner_multisize_len);
			block->rect.y += block->inner_multisize[block->inner_multisize_id].h + INTERLINE;
			block->inner_multisize_id ++;
		}
		block->rect.y += block->inner_multisize[block->inner_multisize_id].floatmargin_top;
		block->rect.x = block->inner_multisize[block->inner_multisize_id].floatmargin_left;
		assert(block->inner_multisize_id < block->inner_multisize_len);
	}

	if (block->rect.x > block->inner_multisize[block->inner_multisize_id].floatmargin_left && node->child_id)
		block->rect.x += INTERWORD;

	SDL_Point pos = (SDL_Point) {
		.x = root->content_pos.x + block->rect.x + node->parent->exospace.left,
		.y = root->content_pos.y + block->rect.y + node->parent->exospace.top };
	switch (node->nodetype) {
	case NODETYPE_TEXT:
		DrawText(rend, node->t, pos);
		break;
	case NODETYPE_FLOWROOT:
		node->fr->content_pos = pos;
		DrawFlowRoot(rend, node->fr);
		break;
	default:
		assert(0);
	}

	block->rect.x += borderbox.w;
	return;
}

void Flow_ResizeInline (FlowRoot *root, Block *block, Inline *in_line) {
	ParentStruct *parent = in_line->node->parent;
	in_line->multisize_len = 0;
	in_line->start_with_nl = false;
	in_line->exospace = in_line->node->parent->exospace;
	in_line->exospace.left += BORDER;
	in_line->exospace.right += BORDER;
	in_line->exospace.top += BORDER;
	in_line->exospace.bottom += BORDER;

	const bool at_start_of_line = block->rect.x == 0;
	int multisize_id = parent->multisize_len - 1;
	if (multisize_id < 0)
		multisize_id = 0;
	Flow_ResizeNodes(root, block, in_line->nodes, in_line->nodes_count);

	/* if nonzero area */
	if (in_line->multisize_len && in_line->multisize[in_line->multisize_len - 1].w) {
		AllocParentMultisize_One(in_line->node->parent);

		in_line->multisize[in_line->multisize_len - 1].w += BORDER + BORDER;
		in_line->multisize[in_line->multisize_len - 1].h += BORDER + BORDER;

		if (!at_start_of_line && !in_line->start_with_nl && in_line->node->child_id) {
		//	block->rect.x += INTERWORD;
			parent->multisize[multisize_id].w += INTERWORD;
		}
		block->rect.x += BORDER + BORDER;

		parent->multisize[parent->multisize_len - 1].w += in_line->multisize[in_line->multisize_len - 1].w;
		if (parent->multisize[parent->multisize_len - 1].h < in_line->multisize[in_line->multisize_len - 1].h)
			parent->multisize[parent->multisize_len - 1].h = in_line->multisize[in_line->multisize_len - 1].h;
	}
	return;
}

void Flow_DrawInline (SDL_Renderer *rend, FlowRoot *root, Block *block, Inline *in_line) {
	SDL_Rect rect = {block->rect.x + in_line->node->parent->exospace.left, block->rect.y + in_line->node->parent->exospace.top, 0, 0};
	int      multisize_id = block->inner_multisize_id;

	if (in_line->start_with_nl) {
		rect.y += block->inner_multisize[multisize_id].h + max(block->inner_multisize[multisize_id + 1].floatmargin_top, INTERLINE);
		rect.x = block->inner_multisize[multisize_id + 1].floatmargin_left + in_line->node->parent->exospace.left;
		multisize_id ++;
		/* child increments block->inner_multisize_id */
	} else if (in_line->node->child_id && in_line->multisize_len && in_line->multisize[0].w) {
		block->rect.x += INTERWORD;
		rect.x += INTERWORD;
	}

	for (int i = 0; i < in_line->multisize_len; ++ i) {
		rect.w = in_line->multisize[i].w;
		rect.h = in_line->multisize[i].h;
		RenderRect(rend, rect, BORDER, (SDL_Color) {31, 127, 31, 255});
		SDL_SetRenderDrawColor(rend, 31, 191, 31, 255);
		SDL_RenderDrawRect(rend, &rect);
		if (i + 1 < in_line->multisize_len) {
			rect.y += max(block->inner_multisize[multisize_id + i].h, rect.h) + max(block->inner_multisize[multisize_id + i + 1].floatmargin_top, INTERLINE);
			rect.x = block->inner_multisize[multisize_id + i + 1].floatmargin_left + in_line->node->parent->exospace.left;
		}
	}

	Flow_DrawNodes(rend, root, block, in_line->nodes, in_line->nodes_count);

	if (in_line->multisize_len && in_line->multisize[in_line->multisize_len - 1].w)
		block->rect.x += BORDER + BORDER;
	return;
}

void Flow_ResizeBlock (FlowRoot *root, Block *block, Block *inner) {
	inner->exospace = inner->node->parent->exospace;
	inner->exospace.left += BORDER;
	inner->exospace.right += BORDER;
	inner->exospace.top = 0;
	inner->exospace.bottom = 0;
	inner->content_size.w = root->content_size.w - inner->exospace.left - inner->exospace.right;
	inner->inner_multisize_id = 0;
	if (inner->inner_multisize_len)
		inner->inner_multisize_len = 0;
	if (inner->content_size.w <= 0) /* TODO: do something */
		return;

	/* We need a new empty line. Do we have one, or do we need to insert one? */
	if (block->rect.x) {
		Flow_InsertNewLine(root, block, inner->node);
		block->rect.y += block->inner_multisize[block->inner_multisize_id].h + INTERLINE;
		block->rect.x = 0;
		block->inner_multisize_id ++;
		assert(block->inner_multisize_id < block->inner_multisize_len);
		Flow_AfterNewLineInserted(root, block);
	}

	inner->rect.y = block->rect.y + inner->node->parent->exospace.top + BORDER;
	inner->rect.x = 0;
	AllocParentMultisize_One((ParentStruct*) inner);
	inner->inner_multisize[0].floatmargin_left = Flow_GetFloatMargin(root, inner, inner->rect.y, 0); /* the height of the block's first line need not be given as the third argument */

	Flow_ResizeNodes(root, inner, inner->nodes, inner->nodes_count);

	inner->borderbox.w = inner->content_size.w + 2 * BORDER;
	inner->borderbox.h = 2 * BORDER;
	for (int line = 0; line < inner->inner_multisize_len; ++ line) {
		if (line || inner->inner_multisize[line].floatmargin_top)
			inner->borderbox.h += max(INTERLINE, inner->inner_multisize[line].floatmargin_top);
		inner->borderbox.h += inner->inner_multisize[line].h;
	}

	Node *child = inner->node;
	Size  child_size = inner->borderbox;
	for (ParentStruct *parent = inner->node->parent; parent; child = parent->node, parent = child->parent) {
		assert(parent->node->flowtype == FLOWTYPE_INLINE || parent->node->flowtype == FLOWTYPE_BLOCK || parent->node->nodetype == NODETYPE_FLOWROOT);
		
		AllocParentMultisize_One(parent);
		assert(parent->multisize[parent->multisize_len - 1].w == 0);
		assert(parent->multisize[parent->multisize_len - 1].h == 0);
		parent->multisize[parent->multisize_len - 1].w = child_size.w;
		parent->multisize[parent->multisize_len - 1].h = child_size.h;
		if (parent->node->flowtype == FLOWTYPE_INLINE) {
			parent->multisize[parent->multisize_len - 1].w += BORDER + BORDER;
			parent->multisize[parent->multisize_len - 1].h += BORDER + BORDER;
		}
		child_size = (Size){
				.w = parent->multisize[parent->multisize_len - 1].w,
				.h = parent->multisize[parent->multisize_len - 1].h};

		AllocParentMultisize_LenPlusOne(parent);
		if (parent->node->flowtype == FLOWTYPE_BLOCK || parent->node->nodetype == NODETYPE_FLOWROOT)
			break;
	}
	block->rect.y += inner->borderbox.h + INTERLINE;
	block->inner_multisize_id ++;

	return;
}

void Flow_DrawBlock (SDL_Renderer *rend, FlowRoot *root, Block *block, Block *inner) {
	if (inner->content_size.w <= 0) /* TODO: do something */
		return;

	/* We need a new empty line. Do we have one, or do we need to insert one? */
	if (block->rect.x > 0) {
		assert(block->inner_multisize_id < block->inner_multisize_len);
		block->rect.y += block->inner_multisize[block->inner_multisize_id].h + INTERLINE;
		block->rect.x = inner->inner_multisize_len ? inner->inner_multisize[0].floatmargin_left : 0;
		block->inner_multisize_id ++;
		assert(block->inner_multisize_id < block->inner_multisize_len);
	}

	inner->rect.x = inner->inner_multisize_len ? inner->inner_multisize[0].floatmargin_left : 0;
	inner->rect.y = block->rect.y + inner->node->parent->exospace.top + BORDER;
	inner->inner_multisize_id = 0;
	SDL_Rect border_rect = {inner->exospace.left - BORDER, block->rect.y + inner->node->parent->exospace.top, inner->borderbox.w, inner->borderbox.h};
	RenderRect(rend, border_rect, BORDER, (SDL_Color) {63, 63, 191, 255});
	SDL_SetRenderDrawColor(rend, 91, 91, 255, 255);
	SDL_RenderDrawRect(rend, &border_rect);

	Flow_DrawNodes(rend, root, inner, inner->nodes, inner->nodes_count);

	block->rect.y += inner->node->parent->exospace.top + inner->borderbox.h + inner->node->parent->exospace.bottom + INTERLINE;
	block->rect.x = 0;
	block->inner_multisize_id ++;
	assert(block->inner_multisize_id < block->inner_multisize_len);
	return; 
}

void Flow_ResizeFloat (FlowRoot *root, Block *block, FloatStruct *floatnode) {
	Size borderbox;
	switch (floatnode->node->nodetype) {
	case NODETYPE_TEXT:
		ResizeText(floatnode->node->t);
		borderbox = floatnode->node->t->size;
		break;
	case NODETYPE_FLOWROOT:
		floatnode->node->fr->borderbox.w = root->content_size.w - floatnode->node->parent->exospace.left - floatnode->node->parent->exospace.right;
		floatnode->node->fr->borderbox.h = root->content_size.h - floatnode->node->parent->exospace.top - floatnode->node->parent->exospace.bottom;
		Free_ResizeFlowRoot(floatnode->node->fr);
		borderbox = floatnode->node->fr->borderbox;
		break;
	default:
		assert(0);
	}

	FloatData data = (FloatData){.x1 = Flow_GetFloatMargin(root, NULL, block->rect.y, borderbox.h), .y1 = block->rect.y};
	if (root->floatdata_len) {
		const FloatData *prev = root->floatdata + root->floatdata_len - 1;
		if (data.y1 < prev->y1)
			data.y1 = prev->y1;
		if (data.y1 <= prev->y2) {
			data.x1 = prev->x2;
			data.x2 = data.x1 + borderbox.w;
			if (max(data.x2, floatnode->node->parent->exospace.left) + floatnode->node->parent->exospace.right > root->content_size.w) {
				data.y1 = Flow_GetShortFloatMargin(root, prev->y2, borderbox.h, block->exospace.left + block->content_size.w - floatnode->node->parent->exospace.left - floatnode->node->parent->exospace.right + block->exospace.right);
				data.x1 = Flow_GetFloatMargin(root, NULL, data.y1, borderbox.h);
			}
		}
	}

	if (data.x1 < floatnode->node->parent->exospace.left)
		data.x1 = floatnode->node->parent->exospace.left;
	data.x2 = data.x1 + borderbox.w;
	data.y2 = data.y1 + borderbox.h;
	floatnode->floatmargin_left = data.x1;
	if ((root->floatdata_len
			&& root->floatdata[root->floatdata_len - 1].node->floatmargin_top == -1)
			|| (block->rect.x > 0
			&& block->inner_multisize_len
			&& block->rect.x > block->inner_multisize[block->inner_multisize_id].floatmargin_left
			&& max(data.x2, floatnode->node->parent->exospace.left) + block->rect.x + floatnode->node->parent->exospace.right > root->content_size.w)) {
		floatnode->floatmargin_top = -1;
	} else {
		floatnode->floatmargin_top = 0;
		if (block->inner_multisize_len && block->exospace.left + block->inner_multisize[block->inner_multisize_id].floatmargin_left < data.x2)
			block->inner_multisize[block->inner_multisize_id].floatmargin_left = data.x2 - block->exospace.left;
	}

	data.node = floatnode;
	AllocFlowrootFloatdata_LenPlusOne(root);
	root->floatdata[root->floatdata_len - 1] = data;
	return;
}

void Flow_DrawFloat (SDL_Renderer *rend, FlowRoot *root, Block *block, FloatStruct *floatnode) {
	SDL_Point pos;
	pos.x = root->content_pos.x + floatnode->floatmargin_left;
	pos.y = root->content_pos.y + block->rect.y + floatnode->floatmargin_top;

	switch (floatnode->node->nodetype) {
	case NODETYPE_TEXT:
		DrawText(rend, floatnode->node->t, pos);
		break;
	case NODETYPE_FLOWROOT:
		floatnode->node->fr->content_pos = pos;
		DrawFlowRoot(rend, floatnode->node->fr);
		break;
	default:
		assert(0);
	}

#ifdef DEBUG
	Size borderbox = floatnode->node->nodetype == NODETYPE_TEXT ? floatnode->node->t->size : floatnode->node->fr->borderbox;
	SDL_Rect border_rect = {pos.x, pos.y, borderbox.w, borderbox.h};
	SDL_Rect marker = {root->content_pos.x + block->exospace.left + block->rect.x, root->content_pos.y + block->rect.y, 10, 10};
	SDL_SetRenderDrawColor(rend, 127, 191, 0, 255);
	SDL_RenderFillRect(rend, &marker);
	SDL_SetRenderDrawColor(rend, 191, 255, 31, 255);
	SDL_RenderDrawRect(rend, &border_rect);
#endif

	return; 
}

void Free_ResizeFlowRoot (FlowRoot *inner) {
	inner->rect.x = 0;
	inner->rect.y = 0;
	inner->content_size = inner->borderbox;
	inner->content_pos = (SDL_Point) {.x = 0, .y = 0};
	inner->exospace = (Sides) {.left = 0, .right = 0, .top = 0, .bottom = 0};
	inner->inner_multisize_len = 0;
	inner->inner_multisize_id = 0;
	inner->floatdata_len = 0;
	inner->all_lines_len = 0;

	if (inner->content_size.w <= 0) /* TODO: do something */
		return;

	Flow_ResizeNodes(inner, (Block*) inner, inner->nodes, inner->nodes_count);

	inner->borderbox.w = 0;
	inner->borderbox.h = 0; //2 * BORDER;
	for (int line = 0; line < inner->inner_multisize_len; ++ line) {
		if (line || inner->inner_multisize[line].floatmargin_top)
			inner->borderbox.h += max(INTERLINE, inner->inner_multisize[line].floatmargin_top);
		inner->borderbox.h += inner->inner_multisize[line].h;
		if (inner->borderbox.w < inner->inner_multisize[line].w)
			inner->borderbox.w = inner->inner_multisize[line].w;
	}
	//inner->borderbox.w += 2 * BORDER;

	return;
}

void Flow_ResizeNode (FlowRoot *root, Block *block, Node *inner) {
	switch (inner->flowtype) {
	case FLOWTYPE_RECT:   Flow_ResizeRect   (root, block, inner);     break;
	case FLOWTYPE_INLINE: Flow_ResizeInline (root, block, inner->fi); break;
	case FLOWTYPE_BLOCK:  Flow_ResizeBlock  (root, block, inner->fb); break;
	case FLOWTYPE_FLOAT:  Flow_ResizeFloat  (root, block, inner->ff); break;
	default:
		printf("Invalid node flowtype %d for ", inner->flowtype);
		for (Node *child = inner; child->parent; child = child->parent->node)
			printf("child %d of ", child->child_id);
		printf("root\n");
		assert(0);
	}
}

void Flow_DrawNode (SDL_Renderer *rend, FlowRoot *root, Block *block, Node *inner) {
	switch (inner->flowtype) {
	case FLOWTYPE_RECT:   Flow_DrawRect   (rend, root, block, inner);     break;
	case FLOWTYPE_INLINE: Flow_DrawInline (rend, root, block, inner->fi); break;
	case FLOWTYPE_BLOCK:  Flow_DrawBlock  (rend, root, block, inner->fb); break;
	case FLOWTYPE_FLOAT:  Flow_DrawFloat  (rend, root, block, inner->ff); break;
	default: assert(0);
	}
}

void Flow_ResizeNodes (FlowRoot *root, Block *block, Node *nodes[], int nodes_count) {
	int current_node = 0;
	for (; current_node < nodes_count; ++ current_node)
		Flow_ResizeNode(root, block, nodes[current_node]);
	return;
}

void Flow_DrawNodes (SDL_Renderer *rend, FlowRoot *root, Block *block, Node *nodes[], int nodes_count) {
	int current_node = 0;
	for (; current_node < nodes_count; ++ current_node)
		Flow_DrawNode(rend, root, block, nodes[current_node]);
	return;
}

/* input: root->borderbox */
void ResizeFlowRoot (FlowRoot *root) {
	root->rect.x = 0;
	root->rect.y = 0;
	root->content_size = root->borderbox;
	root->exospace = (Sides) {.left = 0, .right = 0, .top = 0, .bottom = 0};
	root->inner_multisize_len = 0;
	root->inner_multisize_id = 0;
	root->floatdata_len = 0;
	root->all_lines_len = 0;

	Flow_ResizeNodes(root, (Block*) root, root->nodes, root->nodes_count);
	return;
}

/* input: root->content_pos */
void DrawFlowRoot (SDL_Renderer *rend, FlowRoot *root) {
	root->rect.x = 0;
	root->rect.y = 0;
	root->inner_multisize_id = 0;

	if (root->content_size.w && root->content_size.h)
		Flow_DrawNodes(rend, root, (Block*) root, root->nodes, root->nodes_count);

#ifdef DEBUG
	SDL_SetRenderDrawColor(rend, 127, 0, 127, 255);
	for (int i = 0; i < root->all_lines_len; ++i)
		SDL_RenderDrawRect(rend, root->all_lines + i);
#endif

	return;
}


void DestroyNode (Node *node) {
	switch (node->nodetype) {
	case NODETYPE_INTERNAL:
		break;

	case NODETYPE_TEXT:
		free(node->t);
		break;

	case NODETYPE_FLOWROOT:
		if (node->fr->floatdata)
			free(node->fr->floatdata);
		if (node->fr->all_lines)
			free(node->fr->all_lines);
		if (node->fp->multisize)
			free(node->fp->multisize);
		for (int nodeid = 0; nodeid < node->fp->nodes_count; ++ nodeid)
			DestroyNode(node->fp->nodes[nodeid]);
		free(node->fp->nodes);
		free(node->fp);
		break;
	}

	switch (node->flowtype) {
	case FLOWTYPE_NOT_FLOW:
		break;
	case FLOWTYPE_RECT:
		break;
	case FLOWTYPE_INLINE:
	case FLOWTYPE_BLOCK:
		if (node->fp->multisize)
			free(node->fp->multisize);
		for (int nodeid = 0; nodeid < node->fp->nodes_count; ++ nodeid)
			DestroyNode(node->fp->nodes[nodeid]);
		free(node->fp->nodes);
		free(node->fp);
		break;
	case FLOWTYPE_FLOAT:
		free(node->ff);
		break;
	}

	free(node);
	return;
}


Node *simpleHTMLParser (const char *simpleHTML) {
	const char *a, *b, *s = simpleHTML, *l = s;
	char c;
	int linenumber = 0;
	int w, h;
	ParentStruct *root = NULL, *parent = NULL;
	Node *firstnode = NULL, *node = NULL;

	while (*s) {
		while (isspace(*s))
			if (*s++ == '\n')
				linenumber++, l = s;
		assert(*s == '<');
		c = tolower(*++s);

		if (*s == '/') {
			++s;
			assert(root);
			if (isalpha(*s)) {
				assert(!strncmp(s, parent->name.start, parent->name.end - parent->name.start));
				s += parent->name.end - parent->name.start;
			}
			if (*s != '>') {
				printf("HTML \e[31merror\e[m: expected '\e[94m>\e[m' after '\e[94m</tagname\e[m' at line %d, char %d, got '\e[94m%.5s\e[m...'\n", linenumber, (int)(s - l), s);
				return firstnode;
			}
			++s;
			parent = parent->node->parent;
			continue;
		}

		node = calloc(1, sizeof(Node));
		if (parent) {
			node->parent = parent;
			node->child_id = parent->nodes_count++;
			parent->nodes = realloc(parent->nodes, parent->nodes_count * sizeof(void*));
			parent->nodes[node->child_id] = node;
		}
		if (!firstnode)
			firstnode = node;

		if ('0' <= *s && *s <= '9') {
			w = 0;
			while ('0' <= *s && *s <= '9')
				w = w * 10 + *s++ - '0';
			assert(*s == 'x');
			++s;
			h = 0;
			while ('0' <= *s && *s <= '9')
				h = h * 10 + *s++ - '0';

			node->t = calloc(1, sizeof(TextStruct));
			node->nodetype = NODETYPE_TEXT;
			node->flowtype = parent ? FLOWTYPE_RECT : FLOWTYPE_NOT_FLOW;
			node->t->node = node;
			node->t->size.w = w;
			node->t->size.h = h;

		} else if ('a' <= c && c <= 'z') {
			a = s;
			while ('a' <= c && c <= 'z')
				c = tolower(*++s);
			b = s;

			if (!strncmp(a, "inline", b - a)) {
				node->fi = malloc(sizeof(Inline));
				assert(node->fi);
				bzero(node->fi, sizeof(Inline));
				node->fp = (ParentStruct*) node->fi;
				node->nodetype = NODETYPE_INTERNAL;
				node->flowtype = FLOWTYPE_INLINE;
				if (!parent)
					root = parent = node->fp;

			} else if (!strncmp(a, "block", b - a)) {	
				if (parent) {
					node->fb = malloc(sizeof(Block));
					assert(node->fb);
					bzero(node->fb, sizeof(Block));
					node->fp = (ParentStruct*) node->fb;
					node->nodetype = NODETYPE_INTERNAL;
					node->flowtype = FLOWTYPE_BLOCK;
				} else {
					node->fr = malloc(sizeof(FlowRoot));
					assert(node->fr);
					bzero(node->fr, sizeof(FlowRoot));
					node->fb = (Block*) node->fr;
					root = parent = node->fp = (ParentStruct*) node->fr;
					node->nodetype = NODETYPE_FLOWROOT;
					node->flowtype = FLOWTYPE_NOT_FLOW;
				}
			} else {
				printf("Error: bad tagname: '%.*s'\n", (int)(b - a), a);
				return firstnode;
			}

			node->fp->node = node;
			node->fp->name.start = a;
			node->fp->name.end = b;

		} else
			assert(0);

		while (isspace(*s))
			if (*s++ == '\n')
				linenumber++, l = s;

		c = tolower(*s);
		if ('a' <= c && c <= 'z') {
			a = s;
			while ('a' <= c && c <= 'z')
				c = tolower(*++s);
			b = s;
			if (!strncmp(a, "float", b - a)) {
				node->ff = calloc(1, sizeof(FloatStruct));
				node->ff->node = node;
				switch (node->flowtype) {
				case FLOWTYPE_NOT_FLOW:
				case FLOWTYPE_RECT:
					break;
				case FLOWTYPE_INLINE:
				case FLOWTYPE_BLOCK:
					node->fr = realloc(node->fp, sizeof(FlowRoot));
					assert(node->fr);
					node->fp = (ParentStruct*) node->fr;
					node->fi = NULL;
					node->fb = (Block*) node->fr;
					bzero(((char*)node->fr) + sizeof(ParentStruct), sizeof(FlowRoot) - sizeof(ParentStruct));
					node->nodetype = NODETYPE_FLOWROOT;
					break;
				default:
					assert(0);
				}
				node->flowtype = FLOWTYPE_FLOAT;
			} else if (!strncmp(a, "debug", b - a))
				node->debug_this_node = true;
			else
				printf("Error: bad attribute: '%.*s'\n", (int)(b - a), a);
			while (isspace(*s))
				if (*s++ == '\n')
					linenumber++, l = s;
		}

		assert(*s == '/' || *s == '>');
		if (*s == '/') {
			++s;
			assert(*s == '>');
			++s;
		} else if (*s == '>') {
			++s;
			if (node->flowtype != FLOWTYPE_INLINE && node->flowtype != FLOWTYPE_BLOCK && node->flowtype != FLOWTYPE_FLOAT && node->nodetype != NODETYPE_FLOWROOT) {
				printf("HTML \e[31merror\e[m: Badly terminated rectangle tag: '\e[94m>\e[m' must be preceeded with '\e[94m/\e[m'\n");
				return firstnode;
			}
			parent = node->fp;
		}

	}
	if (parent) {
		printf("\e[31msimpleHTML error\e[m\n");
	}
	assert(!parent);
	return firstnode;
	/* possible memory leak, the other top-level nodes aren't freed */
	/* meh */
}


SDL_HitTestResult HitTest (SDL_Window *window, const SDL_Point *mouse, void *userdata) {
	/* In practice this will resize according to the closest corner.  Good.  Less work for me :P */
	//return SDL_HITTEST_RESIZE_BOTTOMRIGHT;
	return SDL_HITTEST_RESIZE_BOTTOM;
}

int main (int argc, char *argv[], char *envp[]) {
	char wintitle[256];
	snprintf(wintitle, 256, "ViewHTML %d", getpid());
	SDL_Init(SDL_INIT_VIDEO);
	int win_initial_width = 1440;
	int win_initial_height = 900;
	win_initial_width = 832;
	SDL_Window *win = SDL_CreateWindow(wintitle, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, win_initial_width, win_initial_height, SDL_WINDOW_RESIZABLE);
	assert(win);
	SDL_Renderer *rend = SDL_CreateRenderer(win, -1, 0);
	assert(rend);
	SDL_SetWindowHitTest(win, &HitTest, NULL);
	SDL_Event event;
	Size winsize;
	bool cont = true;

	Node *root = simpleHTMLParser("\
		<block>\
			<block>\
				<300x200 float/>\
				<145x37/><73x45/><68x48/><131x33 debug/><120x39/>\
				<inline>\
					<57x48/><84x41/><132x44/>\
					<inline><56x33/><117x36/><100x30/><105x39/><124x34/><73x49/></inline>\
					<inline><57x36/><112x33/><96x50/></inline>\
				</inline>\
				<51x42/><144x41 debug/><83x34/><50x43/>\
				<block>\
					<145x37/><73x45/><68x48/><131x33/><120x39/>\
					<inline>\
						<inline><57x48/><84x41/><132x44 debug/></inline>\
						<56x33/><117x36/><100x30/>\
						<inline><105x39/><124x34/><73x49/></inline>\
						<57x36/><112x33/><96x50/>\
						<block>\
							<1000x100/>\
							<57x48/><84x41/><132x44/>\
							<inline><56x33/><117x36/><100x30/><105x39/><124x34/><73x49/></inline>\
							<57x36/><112x33/><96x50/>\
						</block>\
					</inline>\
					<51x42/><144x41/><83x34/><50x43/>\
					<68x50/>\
				</block>\
				<65x49/>\
			</block>\
			<65x39/><62x16/><54x28/><41x16/><30x16/><65x11/><49x27/><38x17/><36x20/><64x14/><39x23/><34x19/><30x16/><37x18/><55x28/><39x15/><68x21/><32x28/><31x14/><59x20/><40x10/><38x12/><57x19/><45x14/><63x14/><65x23/><44x10/><58x17/><42x10/><36x22/><49x15/><41x25/><69x18/><38x28/><31x20/><51x15/><37x10/><33x25/><30x19/><52x29/><49x10/><40x28/><58x28/><33x29/><33x29/><67x28/><69x16/><52x27/><36x27/><42x13/><61x29/><37x22/><48x12/><63x29/><37x26/><55x18/><62x27/><52x14/><41x12/><66x29/><61x19/><69x13/><60x14/><69x10/><64x21/><51x11/><60x14/><65x22/><33x10/><50x16/><55x10/><39x15/><44x24/><33x16/><34x11/><41x21/><54x25/><48x29/><56x25/><63x25/><36x16/><55x12/><56x23/><45x15/><69x10/><68x22/><53x12/><45x19/><48x10/><41x22/><33x11/><44x13/><39x23/><65x14/><43x17/><41x21/><34x11/><49x29/><56x21/><44x27/><60x16/><116x38/><53x48 debug/><90x38/><123x36/>\
			<block float>\
				<50x170/><40x40/><50x90/>\
			</block>\
			<68x112 float/>\
			<48x24/><56x26/><54x19/><39x20/><66x14/><53x19/><43x12/><38x22/><42x20/><51x17/><66x16/><30x20/><68x18/><33x24/><45x17/><65x23/><68x12/><64x29/><34x11/><51x12/><31x17/><39x16/><66x25/><61x16/><43x17/><52x11/><52x27/><65x23/><42x19/><67x29/><36x26/><68x29/><49x10/><63x29/><55x21/><62x20/><48x16/><34x23/><67x28/><68x20/><66x26/><48x12/><45x26/><47x28/><33x12/><68x26/><45x25/><51x28/><42x12/><58x19/><55x14/><41x17/><58x27/><56x19/><61x22/><40x20/><33x19/><31x25/><65x24/><54x29/><45x24/><47x21/><53x27/><65x22/><47x10/>\
			<23x23 float/>\
			<46x16/><52x16/><36x14/><66x18/><36x29/><31x17/><59x19/><43x17/><53x14/><67x25/><48x16/><55x24/><36x25/><65x15/><57x12/><58x12/><67x24/><46x10/><52x28/><53x29/><63x26/><39x28/><56x16/><31x27/><52x19/><58x15/><65x22/><64x27/><59x15/><42x13/><67x20/><35x10/><69x20/><43x20/><63x12/><42x27/><45x25/><59x15/><65x13/><57x24/><67x28/><56x22/><35x14/><38x17/><68x12/><54x11/><34x19/><41x18/><39x29/><62x18/><44x11/><64x25/><41x24/><32x26/><55x25/><55x28/><47x21/><53x12/><38x24/><62x19/><44x15/><59x19/><60x21/><35x14/><42x15/><65x29/><57x25/><46x20/><45x27/><63x26/><42x10/><68x13/><66x18/><61x29/><45x17/><69x21/><32x14/><49x25/><38x21/>\
			<30x80 float/>\
			<62x28/><54x25/><43x23/><38x20/><57x26/><58x15/><37x16/><46x29/><44x20/><49x10/><41x15/><36x20/><67x28/><65x24/><30x28/><56x22/><35x25/><50x21/><68x20/><65x21/><66x24/><61x19/><61x22/><44x18/><65x16/><61x21/><34x29/><42x23/><60x13/><52x18/><62x24/><58x17/><50x28/><68x27/><39x23/><69x14/><53x10/><30x11/><55x21/><46x12/><67x13/><49x20/><64x14/><59x16/><64x13/><42x15/><58x26/><69x24/><49x19/><43x16/><43x22/><60x15/><64x11/><31x19/><66x14/><50x15/>\
			<846x174/>\
		</block>");
	assert(root->nodetype == NODETYPE_FLOWROOT);
	root->fr->content_pos = (SDL_Point) {.x = 0, .y = 0};

	bool render_log = false;
	while (cont) {
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				cont = false;
				break;
			} else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_l)
				debug_log = render_log = true;
		}

		SDL_GetRendererOutputSize(rend, &winsize.w, &winsize.h);
#ifdef DEBUG
		winsize.w -= 200; /* to see that nothing gets drawn outside */
#endif
		if (winsize.w < 0)
			winsize.w = 0;
		bool resized = root->fr->borderbox.w != winsize.w || root->fr->borderbox.h != winsize.h;
		if (!resized) {
			/* In case of an SDL_WINDOWEVENT_SHOWN or EXPOSED and we don't use a compositing wm */
			/* Presenting before redrawing after a resize, makes SDL/X center the old-sized buffer on the new-sized clip */
			SDL_RenderPresent(rend);
		}

		SDL_SetRenderDrawColor(rend, 0, 0, 0, 255);
		SDL_RenderClear(rend);
		SDL_Rect rect = {0, 0, winsize.w, winsize.h};
		SDL_SetRenderDrawColor(rend, 255, 255, 255, 255);
		SDL_RenderDrawRect(rend, &rect);

		if (resized) {
			snprintf(wintitle, 256, "ViewHTML %d %dx%d", getpid(), winsize.w, winsize.h);
			SDL_SetWindowTitle(win, wintitle);

			root->fr->borderbox = winsize;
			ResizeFlowRoot(root->fr);
#ifdef LOG
			if (false) {
				for (int i = 0; i < root->fb->inner_multisize_len; ++i)
					printf("[main] root->inner_multisize[%d] = {w = %d, h = %d, left = %d, right = %d}\n", i, root->fb->inner_multisize[i].w, root->fb->inner_multisize[i].h, root->fb->inner_multisize[i].floatmargin_left, root->fb->inner_multisize[i].floatmargin_right);
				Block *block = root->fb->nodes[0]->fb;
				for (int i = 0; i < block->inner_multisize_len; ++i)
					printf("[main] root[0]->inner_multisize[%d] = {w = %d, h = %d, left = %d, right = %d}\n", i, block->inner_multisize[i].w, block->inner_multisize[i].h, block->inner_multisize[i].floatmargin_left, block->inner_multisize[i].floatmargin_right);
				debug_log = render_log = false;
			}
#endif
		}

		DrawFlowRoot(rend, root->fr);

#ifdef DEBUG
		SDL_SetRenderDrawColor(rend, 127, 127, 127, 255);
		for (int i = 0; i < winsize.h / 200; ++i) {
			SDL_Rect rect = {.x = winsize.w, .y = i * 200 + 100, .w = 10, .h = 100};
			SDL_RenderFillRect(rend, &rect);
		}
		for (int i = 0; i < winsize.h / 20; ++i) {
			SDL_Rect rect = {.x = winsize.w + 10, .y = i * 20 + 10, .w = 10, .h = 10};
			SDL_RenderFillRect(rend, &rect);
		}
#endif

		SDL_RenderPresent(rend);
		SDL_WaitEvent(NULL);
	}

	DestroyNode((Node*) root);

	SDL_DestroyRenderer(rend);
	SDL_DestroyWindow(win);
	SDL_Quit();
	return 0;
}

/* vim: set noexpandtab filetype=viewhtml: */
