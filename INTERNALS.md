Implementation internals
=========================

Invariants
-----------

* Drawing functions don't change the `size`, `content_size`, `borderbox_size` and `[inner_]multisize*` members.
* Resizing functions can change them.

Here, the 'element' will be the element that is being resized.
* At the start of a resize function, the element's `[inner_]multisize` is reset to an appropriate zero.
* At the start of a resize function, none of the element's children are included in its parent's `size`.
* When an element's descendant elements are working on sizing a line,
  all of the previous lines' sizes are known to the element and the
  lines' parts' sizes are known to the elements that contain these
  parts.
* A line's part doesn't have to cover the full line's height.
* At the end of a resize function, the element's size is included
  in its parent's `[inner_]multisize` and `size`, but not yet in its
  gandparent's `[inner_]multisize` and `size`.

* Elements with more than 1 line usually won't have any blank lines.
* When an in-flow node starts a new line, all of its parent elements
  that didn't have any size before this node together with all of
  their empty nodes before this node also start at the new line.

Currently, there is no difference between border and padding, it's one thing
represented by the macro BORDER and it's rendered as a 1px border and
BORDER-1px padding.

Style
------

* `sizeof(pointer to something)` gets written as `sizeof(void*)` to avoid confusion with `sizeof(something)`
* No assignments are made in assert, printf and debug statements.
