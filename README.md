ViewHTML - The core of a to-be web browser
==================================================

An HTML viewer written in C with SDL.


Why start a new engine?
--------------------------------------------------

To ensure security and privacy by opt-in execution of server commands, not opt-out execution as is done in a browser + extensions/plugins model.


Goals
--------------------------------------------------

The main point is to give the client as much control as possible:
* allow the client to view the same content at any level of GUIness and visual atractiveness (with plaintext, netcat and encrypted copy at one end, Gopher and Gemini near this end and Web 9999 at the other end),
* give advanced users a shell,
* give beginners an oportunity to learn – don't separate using from configuring and programming,
* reuse code, reuse configs: allow easy config sharing by storing them as text,
* allow arbitrary changing of server-supplied data, including:
    - content (eg. merging paged content into one document, greping content into SQL tables),
    - style (presentation; eg. making HTML or CSS adjustments, removing cookie popups and banners),
    - logic (changing JS, adding site-specific keyboard shortcuts),
    - URLs (eg. removing tracking ?query key-value pairs, redirecting YouTube to a proxy)
* both by static file substitution and dynamic amendments (eg. adding own style sheets, removing CSS rules);
* wide support:
    - support many file formats, including markup formats, Haml, XSL,
    - support server-side abstract formats and languages, like XSL, jQuery, perhaps a few JS frameworks,
    - support the HyperText Web, GopherSpace, GeminiSpace, and the mixing of theese,
    - support many output types: screen (with projector enhancments), terminal alt buffer, command line, screen reader,
    - multilingual support,
* limited implementation of the graphical session's and/or window manager's tasks: (low priority goals)
    - tabs,
    - run on the Linux/Unix FrameBuffer or Direct Rendering Manager,
    - emulate a terminal and allow for embedded images/videos (nothing more: no font size changes or anything),
    - trapezium transformation of the entire viewport (for projectors).

While the primary user interface is dedicated to advanced users oparating on a desktop (operating with a keyboard), a typical interface with graphical configuration (inspired by μMatrix) will also be available.

Users of simpler front-ends will benefit from configuration sheets and static file replacements written by others.


User input
--------------------------------------------------

* Send SDL\_QUIT (x the window or press Ctrl-C in the terminal) to quit.
* Change the window size.
* Press l to print some logs to stdout.


Proto-HTML
--------------------------------------------------

Tags:
* \<block> ... \</block>
* \<inline> ... \</inline>
* \<WIDTHxHEIGHT/>

Attributes:
* float – left float
* debug

Whitespace is ignored.


Compiling
--------------------------------------------------

`make`


Contributing
--------------------------------------------------

Contributions are welcome  :)

Before you fork please notify me, because I work mainly off-git and rarely push to this repo.

Help is needed with testing (writing protoHTML files).

What language should the browser proper be written in?  Feel free to suggest languages!


Project components
--------------------------------------------------

In the project we will (we want to):
* implement a browser engine,
* implement a monolithical[1] browser,
* develop guides on writing user-friendly and accessible web pages,
* propose changes to HTML and CSS,
* maintain a repository with configuration sheets,
* maintain a repository of static file substitutions, mainly JavaScript files with tracking parts cut out,
* contribute to major server-side software implementations to include a Gopher or Gemini frontend.

[1] not discouraging plugins; open for discussion


The 'Carrige skip' feature
--------------------------------------------------

* It's to be implemented natively in the flow algorithm
* Will be passed on transparently through tables, grids, flex containers
* opaque elements like images will be eg. cut and copied

* Grid regions don't need to be rectangular.
* The user will be able to mark rectangular portions of the viewport illegible (yeah it will be slow).  Usefull for projectors.

We propose changes to the HTML and CSS standards:
--------------------------------------------------

@media tty shouldn't be deprecated.

ch length unit - the size of 1 character (@media tty only)

New tags:
\<local> local content - content unique to the URL path.
\<shared> content shared between multiple paths.

\<box> A div with significance to the document structure, a general sectioning tag.  MAY be used in place of sectioning tags like \<section> or \<article>, though the use of more specific tags is encouraged.  MAY, but DOESN'T NEED TO be styled with borders or a background marking it seperate from it's surrounding content.  The use of the aria-label attribute is encouraged.
\<transparent> A div without major significance to the document structure.  CAN'T be styled with borders or a background marking it seperate from it's surrounding content.  MAY be styled with padding, slight background color changes.  CAN'T have the attribute aria-label.

\<alt> A tag accepting the alt attribute.  (It MUST have the alt attribute.)  The content should be replaced by the alternative text when the user agent isn't able to display images in-line (a graphical screen browser not implementing image rendering is said to be able to display images; a terminal browser allowing to download images and display them using a chosen program is said to not be able to display images in-line).  This tag should be used for ASCII art and Unicode art.

The \<pre> tag should accept the alt attribute under the same rules as the \<alt> tag except that the attribute is optional for the \<pre> tag.


We establish guides on styling web pages:
--------------------------------------------------

* Make styling optional for the client.  The website should look OK without CSS enabled and with only partial CSS support.
* Use display:grid for layout (top, bottom and side bars).  Don't use padding for layout.  Don't use positioned non-empty divs that would overlap if the padding was removed.
* Avoid HTML/CSS tricks.
* Don't use JavaScipt to load content unique to the URL path.
* Avoid padding and margins for the TTY @media type.
* Write correct markup, write correct XHTML.
* Don't overuse classes.
