release: target/viewhtml
debug:   target/viewhtml-d
both:    release debug

target/viewhtml: viewhtml.c
	cc -g -Wall viewhtml.c -lSDL2 -o target/viewhtml

target/viewhtml-d: viewhtml.c
	cc -g -DDEBUG -Wall viewhtml.c -lSDL2 -o target/viewhtml-d

