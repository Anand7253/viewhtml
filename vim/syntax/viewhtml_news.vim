syntax iskeyword @,45,46,48-57,A-Z,[,],_,a-z
syntax keyword Special vTHISCOMMIT
syntax match Special /\<v[0-9a-f]\{7,40\}\>/
syntax match Type    /\<20\d\d-\d\d-\d\d\>/
syntax match Keyword /\[[bf]\d\+\]/

let b:current_syntax = 'viewhtml_news'
