vTHISCOMMIT, 2024-05-01
* flow:
  - a block can be inside an inline (mostly fixed [b1])
  - interword
  - fixed [b6]
* floats:
  - they may start below the line in which they are defined
  - x1 and x2 are now counted from the flow root's left, not the block's left (fixed [b3])
* major changes:
  - offset -> exospace
  - separated nodetype and flowtype (fixed [b2] and [f2])
  - struct Node isn't an inheritance base any more, now has pointers to nodetype- and flowtype-specific data
* SDL hittest
* fixed memory errors caused by bad bzero call
* c99 standard compliance
[b1] [b4]

v2dfb96d, 2023-12-27
* Offset moved from function arguments to the structures (ParentNode, Inline, Block, FloatBlock, FlowRoot)
* Removed Inline::newline_start_x
* Bug fixed: previously in_line's borders were drawn using the floatmargin_left of the in_line's first line (effected only multiline in_lines)
* If text doesn't fit beside a float it gets pushed below the float (added FlowLine::floatmargin_top) (doesn't apply for inlines yet [b6])
* The first line of a block is now pushed accordingly to the line's y position, not the block's y position
* Removed dead code in InsertNewLineInBlock and in DrawInlineInBlock
* Fixed [b5]
[b1] [b2] [f2] [b3] [b4] [b6]

* Fixed markup
* Added NEWS and BUGS, and their syntax highlighting for vim
* Moved DOC.md to README.md


v3c948d5, 2023-12-19
* Everything added
* A block can't be inside an inline tag [b1]
* Overflows aren't handled yet (overflow:visible if block's content-box.width > 0, overflow:hidden and border:transparent otherwise) [f1]
* Floats don't create a new formatting context (NFC), they are like a block instead of flowroot [b2]
* <WxH float> is missing [f2]
* NODETYPE_FLOAT_LEFT really combines NODETYPE_BLOCK (should be flowroot, see [b2]) and css 'float: left' [f2]
* Floats are only left-floats (start-floats) [f3]
* The left borders of blocks inside which a float is located are included twice in the float_margin [b3]
* The line directly succeding a block isn't pushed by floats [b4]
* The float's anchors are rendered in random colours [b5]



/* vim: set filetype=viewhtml_news: */
